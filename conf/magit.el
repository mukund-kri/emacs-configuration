;;;; Magit specific configuration

(setq
  magit-repository-directories
  '(
    ;; My main work area
    ("~/workarea/" . 4) 
    
    ;; My repos on gitlab mostly my teaching material
    ("~/projects/" . 2)
   
    ;; Orgs
    ("~/orgs/" . 1)

    ;; My emacs / spacemacs config
    ("~/.emacs.d/" . 1)

    ;; My cookicutters and gitignores
    ("~/.cookiecutters" . 1)
    ("~/.gitignores" . 1)
    )

  ;; Magit-list-repository list format
  magit-repolist-columns
  '(("Name"    25 magit-repolist-column-ident                  ())
    ("Version" 25 magit-repolist-column-version                ())
    ("D"        1 magit-repolist-column-dirty                  ())
    ("L<U"      3 magit-repolist-column-unpulled-from-upstream ((:right-align t)))
    ("L>U"      3 magit-repolist-column-unpushed-to-upstream   ((:right-align t)))
    ("Path"    99 magit-repolist-column-path                   ()))
  )
 
