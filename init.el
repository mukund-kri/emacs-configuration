
;; Configuration base dir
(defvar config-dir (expand-file-name "~/.emacs.d"))


;; Configure
(load (expand-file-name "conf/packages.el" config-dir))
(load (expand-file-name "conf/ui.el" config-dir))
(load (expand-file-name "conf/keybindings.el" config-dir))
(load (expand-file-name "conf/orgmode.el" config-dir))
(load (expand-file-name "conf/magit.el" config-dir))
(load (expand-file-name "conf/functions.el" config-dir))
;; (load (expand-file-name "conf/merlin.el" config-dir))

;; Configure the emacs-dashboard
(load (expand-file-name "conf/dashboard.el" config-dir))

;; Programming IDEs/Modes setup
;; (load (expand-file-name "conf/prog-cc-mode.el" config-dir))
(load (expand-file-name "conf/prog-lsp-mode.el" config-dir))
(load (expand-file-name "conf/prog-scala-mode.el" config-dir))
(load (expand-file-name "conf/prog-go-mode.el" config-dir))
(load (expand-file-name "conf/prog-rust-mode.el" config-dir))
(load (expand-file-name "conf/prog-python-mode.el" config-dir))

(add-hook 'text-mode-hook 'turn-on-flyspell)
(add-hook 'text-mode-hook 'turn-on-auto-fill)

(setq js-indent-level 2)

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-quickhelp-delay 0)
 '(company-tooltip-align-annotations t)
 '(package-selected-packages
   '(org-roam postframe lsp-metals no-littering rust-mode lsp-dart dart-mode company-lsp lsp-ui lsp-mode cmake-mode company-c-headers company-go go-mode dockerfile-mode yaml-mode elpy erlang all-the-icons dashboard neotree markdown-mode utop merlin ocp-index ocp-indent tuareg caml validate yasnippet-snippets yasnippet magit ivy gruvbox-theme org-bullets org-journal json-mode nix-mode reason-mode haskell-mode powerline projectile use-package)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line
