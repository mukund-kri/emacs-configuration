;;;; Let's start of by killing the menubar, toolbar and scroll
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(show-paren-mode 1)

(set-face-attribute 'default nil :height 110)

(when (version<= "26.0.50" emacs-version )
  (global-display-line-numbers-mode))

(load-theme 'gruvbox t)


;; Wrap lines to 80 chars
(setq-default fill-column 80)


;; (defvar --backup-directory (concat user-emacs-directory "backups"))
;; (if (not (file-exists-p --backup-directory))
;;        (make-directory --backup-directory t))
;; (setq backup-directory-alist `(("." . ,--backup-directory)))
;; (setq make-backup-files t               ; backup of a file the first time it is saved.
;;       backup-by-copying t               ; don't clobber symlinks
;;       version-control t                 ; version numbers for backup files
;;       delete-old-versions t             ; delete excess backup files silently
;;       delete-by-moving-to-trash t
;;       kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
;;       kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
;;       auto-save-default t               ; auto-save every buffer that visits a file
;;       auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
;;       auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
;;       )

(use-package no-littering
  :demand t)


(setq auto-save-file-name-transforms                                                                                  `((".*" ,temporary-file-directory t)))

;;(setq auto-save-file-name-transforms
;;      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

;; turn of autosave. I know its a nuclear option. should be good
;; enough for now.
(setq auto-save-default nil)
