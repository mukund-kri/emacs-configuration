(setq lsp-keymap-prefix "s-l")

(use-package lsp-dart)
(use-package lsp-mode
  :hook (
	 (dart-mode . lsp)
	 (scala-mode . lsp)

	 (lsp-mode . lsp-enable-which-key-integration))
  :commands lsp
  :custom
  (lsp-dart-sdk-dir "~/computer/software/flutter/bin/cache/dart-sdk/")
  (setq lsp-prefer-flymake nil))


(use-package lsp-ui :commands lsp-ui-mode)
(use-package company-lsp :commands company-lsp)

(use-package dart-mode)
