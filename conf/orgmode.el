;; Temp agenda file. Till I get my GTD system online
;; (setq org-agenda-files (list "~/orgs/orgs/todo/days-of-emacs.org"))


;;;; GTD configuration from my old spacemacs file.
;;;; Inspired by :: TODO
(setq org-todo-keywords '((sequence "TODO" "IN_PROGRESS" "|" "DONE" "ABANDONDED")))

(setq org-agenda-files '("~/orgs/orgs/gtd/inbox.org"
			 "~/orgs/orgs/gtd/gtd.org"
			 "~/orgs/orgs/gtd/tickler.org"


			 "~/orgs/orgs/plans/c++/data-munging-project.org"
			 "~/orgs/orgs/projects/days-of-code/site-generator.org"
			 ))

(setq org-capture-templates
      '(
	("t" "GTD[INBOX]" entry
	 (file+headline "~/orgs/orgs/gtd/inbox.org" "Tasks")
	 (file "~/orgs/orgs/templates/todotemplate.txt")
	 :empty-lines 1
	 )
	("Q" "Open Question[NOTES]" entry
	 (file+headline "~/orgs/orgs/notes/open-questions.org" "Open Questions")
	 (file "~/orgs/orgs/templates/open-question.txt")
	 :empty-lines 1
	 )
	("c" "New Concept[NOTES]" entry
	 (file+headline "~/orgs/orgs/notes/new-concepts.org" "New Concepts")
	 (file "~/orgs/orgs/templates/new-concept.txt")
	 :empty-lines 1
	 )
	))


(setq org-refile-targets '(("~/orgs/orgs/gtd/gtd.org" :maxlevel . 2)
                           ("~/orgs/orgs/gtd/someday.org" :maxlevel . 2)
                           ("~/orgs/orgs/gtd/on-hold.org" :level . 1)
                           ("~/orgs/orgs/gtd/tickler.org" :maxlevel . 2)
			   ("~/orgs/orgs/gtd/abandoned.org" :maxlevel . 1)
			   ("~/orgs/orgs/gtd/done-archive.org" :maxlevel . 2)
                           ))


(use-package ob-dart)
(add-to-list 'org-babel-load-languages  '(dart . t))

(setq-default fill-column 80)
