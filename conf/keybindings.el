;;;; CONFLICTS WITH ORG MODE 
;;(when (fboundp 'windmove-default-keybindings)
;;  (windmove-default-keybindings))


(global-set-key (kbd "C-<left>")  'windmove-left)
(global-set-key (kbd "C-<right>") 'windmove-right)
(global-set-key (kbd "C-<up>")    'windmove-up)
(global-set-key (kbd "C-<down>")  'windmove-down)


(global-set-key (kbd "C-c b") 'ivy-switch-buffer)
(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c c") 'org-capture)


(global-set-key (kbd "C-c q") 'auto-fill-mode)


;;;; Coding efficiency
(global-set-key (kbd "C-c f") 'xref-find-definitions-other-frame)
(global-set-key (kbd "C-c p") 'xref-pop-marker-stack)

