
(use-package go-mode
  :ensure t
  :init)


;; Completion integration
(use-package company-go
  :ensure t
  :after go
  :config
  (setq tab-width 4)

  :bind (:map go-mode-map
	      ("M-." . godef-jump)))


(add-hook 'go-mode-hook
	  (lambda ()
	    (setq tab-width 4)))
