;; define function to shutdown emacs server instance
(defun server-shutdown ()
  "Save buffers, Quit, and Shutdown (kill) server"
  (interactive)
  (save-some-buffers)
  (kill-emacs)
  )


;;;; A whole bunch of generators for org files from TODO lists, project plans
;;;; day logs and much more
(defun org-file-name (type)
  "Return a file name based on type and current date"
  (pcase type
    ('day-log (format-time-string "%Y.%m.%d.org"))
    ('week-plan (format-time-string "%Y/%m.%d.org"))
    ('pomodoro (format-time-string "%Y/%m/%d.org"))
    ('project (format "%s.org" (read-string "Enter project name: ")))
    (_ "default.org")
    )
  )

(defun generate-date-based-org (base-dir type yas-template-name)
  "Create file with name based on date, make it default buffer, insert header
yas template and expand"
  (setq file-path (concat base-dir (org-file-name type)))
  (switch-to-buffer (find-file-noselect file-path))
  (org-mode)
  (insert yas-template-name)
  (yas-expand))


(defun doe-daylog ()
  "Short cut to generate a day log for my 100 day challenges"
  (interactive)
  (generate-date-based-org "~/orgs/100-days-of-emacs/days/" 'day-log "100doeday")
  )

(defun week-plan ()
  "week plan generator function"
  (interactive)
  (generate-date-based-org "~/orgs/orgs/week.tasks/" 'week-plan "planw")
  )

(defun mini-project ()
  (interactive)
  "Create a plan for a mini project"
  (generate-date-based-org "~/orgs/orgs/projects/backlog/" 'project "minip")
  )

(defun nix-daylog ()
  "Generate a org file for the day log for 30 days of nix challenges"
  (interactive)
  (generate-date-based-org "~/orgs/30-days-of-nix/days/" 'day-log "nixday")
  )

(defun pomodoro-day ()
  "Blank template for day's pomodoro"
  (interactive)
  (generate-date-based-org "~/orgs/orgs/pomodoro/" 'pomodoro "pom-day")
  )

(defun ocaml-day ()
  "Generate day log for learning ocaml"
  (interactive)
  (generate-date-based-org "~/workspace/language/ocaml/100-days-of-ocaml/daylog/" 'day-log "ocaml-day")
  )


(defun eguide ()
  "Open up my emacs guide"
  (interactive)
  (let ((default-directory "~/orgs/orgs/guides/emacs"))
    (find-file "index.org")
    (neotree-show)
    (neotree-dir default-directory)))


(defun guide (name)
  "Open up a guide"
  (interactive "sName of guide: ")
  (let ((default-directory (concat "~/orgs/orgs/guides/" name)))
    (neotree-show)
    (neotree-dir default-directory)
    (find-file "index.org")))

(defun reinstall-all ()
  "Refresh and reinstall all activated pacages"
  (interactive)
  (package-refresh-contents)
  (dolist (package-name package-activated-list)
    (when (package-installed-p package-name)
      (unless (ignore-errors
		(package-reinstall package-name))
	(warn "Package %s failed to install" package-name)))))
