# My current emacs config

## Why?

Because I can in emacs! 

Well in all seriousness this is what emacs was built for. Slowly
configuring/scripting the editor to users' quirks. 

## History

This is my third major attempt at a custom emacs config. 

The first one was a collection of small snippets all collected in one
init.el. This worked for me for a long time (~10 years)

Next up I got more serious about my working env and I went all in with
*Spacemacs*. I even created a private layer to suite my needs. The
code is here ...
https://gitlab.com/mukund-kri/my-spacemacs-config

Spacemacs looks awesome, and is a great idea overall but its getting
clunky and heavy these days. Its mostly a pain get up and running in a
new system (mainly due to package management). So I started my own
config system from scratch.

## Features 

* A thin `init.el` which mostly just loads the files from the `conf`
  folder. 
* All packages are loaded via `use_package`.

