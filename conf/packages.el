;;;; PACKAGE MANAGEMENT
(setq package-archives
      '(("melpa"        . "https://melpa.org/packages/")
        ("melpa-stable" . "https://stable.melpa.org/packages/")
	("gnu"          . "https://elpa.gnu.org/packages/")))

(package-initialize)

;; Initialize use-package on fresh systems
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; need use-package for further work
(use-package use-package
  :config
  (setq use-package-always-ensure t))

(setq use-package-always-defer t
      use-package-always-ensure t
      backup-directory-alist `((".*" . ,temporary-file-directory))
      auto-save-file-name-transforms `((".*" ,temporary-file-directory t)))

;;;; Look and feel packages
(use-package powerline
  :init
  (powerline-default-theme))

(use-package gruvbox-theme)


;;;; Org-Mode
(use-package org-journal
  :config
  (setq org-journal-dir "~/orgs/orgs/org.journal")
  (setq org-journal-file-format "%Y%m%d.org"))

(use-package org-bullets
    :hook (org-mode . org-bullets-mode))


;;;; Productivity
(use-package projectile
  :config
  (projectile-mode))

(use-package ivy
  :config
  (ivy-mode 1)
  (setq ivy-use-virtual-buffers t)
  (setq enable-recursive-minibuffers t))

(use-package magit)

(use-package company
  :ensure t
  :custom
  (company-quickhelp-delay 0)
  (company-tooltip-align-annotations t)
  :hook
  ((prog-mode utop-mode) . company-mode)
  :config
  (company-quickhelp-mode 1)
  :bind
  ("M-o" . company-complete)
  )

(use-package company-quickhelp
  :ensure t
  :bind (:map company-active-map
              ("M-h" . company-quickhelp-manual-begin))
  )

(use-package neotree
  :config
  (global-set-key [f8] 'neotree-toggle)
  )

(use-package all-the-icons)

(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))

;;;; Programming language modes
(use-package sml-mode)
(use-package haskell-mode)
(use-package reason-mode)
(use-package json-mode)
(use-package nix-mode
  :mode "\\.nix\\'")

(use-package markdown-mode
  :ensure t
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.MD\\'" . gfm-mode)
	 ("\\.md\\'" . markdown-mode)
	 ("\\.markdown\\'" . markdown-mode))
  :init (setq markdown-command "multimarkdown"))

(use-package yaml-mode)
(use-package dockerfile-mode)

(use-package erlang
  :init
  (add-to-list 'auto-mode-alist '("\\.P\\'" . erlang-mode))
  (add-to-list 'auto-mode-alist '("\\.E\\'" . erlang-mode))
  (add-to-list 'auto-mode-alist '("\\.S\\'" . erlang-mode))
  :config
  (add-hook 'erlang-mode-hook
            (lambda ()
              (setq mode-name "erl"
                    erlang-compile-extra-opts '((i . "../include"))
                    ))))

(use-package yasnippet
  :ensure t
  :config
  (setq
   yas-verboisty 1
   yas-wrap-around-region t)

  (with-eval-after-load 'yasnippet
    (setq yas-snippet-dirs '("~/.emacs.d/conf/snippets"
			     "~/.emacs.d/elpa/yasnippet-snippets-20190725.1049/snippets")))

  (yas-reload-all)
  (yas-global-mode))

(use-package yasnippet-snippets
  :ensure t)


;;;; Let' try out org-roam
(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/orgs/orgs/ORG-ROAM")
  :bind (("C-c n l" . org-roam-buffer-toggle)
	 ("C-c n g" . org-roam-graph)
	 ("C-c n f" . org-roam-node-find)
	 ("C-c n i" . org-roam-node-insert))
  :config
  (org-roam-setup))
